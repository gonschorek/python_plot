__author__ = 'Tim Gonschorek'
__model_path__ = '/home/gonschorek/Schreibtisch/dcca_tests/'
__out_path__   = '/home/gonschorek/Schreibtisch/'
__models__     = ['elbtunnel']
import numpy as np
import matplotlib as mpl
import os
import getopt
import sys
import datetime
import numpy as np

## agg backend is used to create plot as a .png file
mpl.use('agg')

import matplotlib.pyplot as plt
from pylab import plot, show, savefig, xlim, figure, \
    hold, ylim, legend, boxplot, setp, axes

mc_dic = {'0': 'iimc',
          '1': 'nuXmv-ic3',
          '2': 'aigbmc',
          '3': 'IC3',
          '4': 'nuXmv-bdd',
          '5': 'nuxmv-bmc',
          '6': 'NuSMV-bbd',
          '7': 'NuSMV-bmc',
          '8': 'k-ind'
          }

def main():
    # parse command line options
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hm:t:d:", ["help","model"], )
    except getopt.error, msg:
        print msg
        print "for help use --help"
        print "model checker: 0 - iimc, 1 - nusmv ..."
        sys.exit(2)

    # process options
    model = ""
    model_checker = -1
    diagram_type = -1

    for o, a in opts:
        if o in ("-h", "--help"):
            print __doc__
            sys.exit(0)
        if o in "-m":
            model = a
        if o in "-t":
            model_checker = a.split(',')
        if o in "-d":
            if a == '0':
                for m in model_checker:
                    single_model_single_mc(model, m), #one model one tool
            if a == '1':
                single_model_all_mc(model),           #all available models single tool
            if a == '2':
                all_model_all_mc()
            if a == '3':
                for m in model_checker:
                    single_runs_eval(model, m)

def single_runs_eval(model, mc):

    single_result_fileS = [file for file in os.listdir(__model_path__+ model + "/" + str(mc) + "/") if (file.find("dccaresult_single") == 0)]
    results =  []
    first_run = True
    for f in single_result_fileS:
        f = open(__model_path__ + model + "/" + str(mc) + "/" + f)
        f.readline()
        lines = f.readlines()
        f.close()
        i = 0

        print f

        for l in lines:
            print i
            print l
            data_split = l.split(";")
            if first_run:
               results.append(float(data_split[2])/10)
            else:
                results[i] += float(data_split[2])/10
            i += 1

        first_run = False

    print results


def all_model_all_mc():
    num_mc = 9
    mc_time = []
    model_per_mc = []
    models = os.listdir(__model_path__)
    print models
    fig = figure() #plt.figure(1, figsize=(9, 6))
    # Create an axes instance
    #    fig.set_size_inches(12.5, 10.5)
    ax = axes() #fig.add_subplot(111)
    for mc in range(0, num_mc):
        mc_time = []

        for model in models:
            complete_result_file = [file for file in os.listdir(__model_path__+ model + "/" + str(mc) + "/") if (file.find("dccaresult_complete") >= 0)]
            if len(complete_result_file) == 0:
                continue

            f = open(__model_path__ + model + "/" + str(mc) + "/" + complete_result_file[0])

            f.readline()
            lines = f.readlines()
            f.close()
            mc_time_tool = []
            for l in lines:
                data_split = l.split(";")
                mc_time_tool.append(float(data_split[3]))
#            print "***" + mc_dic[str(mc)] + " - " + model + "***"
#            print mc_time_tool
            if len(mc_time_tool) == 0:
                mean = 0
            else:
                mean = float(sum(mc_time_tool))/len(mc_time_tool)

#            if mean > 3700 :
#                mean = 3700
            #            print mean
#            if mean >= 3700:
#                mean = 3700
            mc_time.append(mean)
#            bp = ax.plot(mc_time)
#            print mc_dic[str(mc)]
#            print model
#            print mean
            #del mc_time_tool[:]
#        print np.linalg.norm(mc_time)
#        print max(mc_time)
        #norm1 = [t / max(mc_time) for t in mc_time]
        model_per_mc.append(mc_time)

#    fig = plt.figure(figsize=(12, 6))
#    vax = fig.add_subplot(121)
	f_out = open("/home/gonschorek/memory.csv", "w")
    print model_per_mc
	
    for means in model_per_mc:
        f_out.write((";").join([str(m) for m in means]) + "\n")

#    vax.plot([0,1,2,3,4,5,6,7], model_per_mc, 'b^')
#    vax.vlines([0,1,2,3,4,5,6,7], [0], model_per_mc)
#    vax.set_xlabel('time (s)')
#    vax.set_title('Vertical lines demo')
    now = datetime.datetime.now()
    fig.savefig(__out_path__ + 'allMCallModels'+ now.strftime("%Y%m%d_%H%M") + '.png', bbox_inches='tight')


def single_model_all_mc(model):
    model_checker = os.listdir(__model_path__+ model + "/")
    model_checker = [int(m) for m in model_checker]
    model_checker.sort()
    mc_time = []
    for mc in model_checker:
        complete_result_file = [file for file in os.listdir(__model_path__+ model + "/" + str(mc) + "/") if (file.find("dccaresult_complete") >= 0)]
#        f = open(__model_path__ + model + "/tests/" + mc + "/" + complete_result_file[0])
        f = open(__model_path__ + model + "/" + str(mc) + "/" + complete_result_file[0])

        f.readline()
        lines = f.readlines()
        mc_time_tool = []
        for l in lines:
            data_split = l.split(";")
            mc_time_tool.append(float(data_split[2]))

        mc_time.append(mc_time_tool)

    data_to_plot = mc_time
    #  Create a figure instance
    fig = figure() #plt.figure(1, figsize=(9, 6))
    # Create an axes instance
    fig.set_size_inches(12.5, 10.5)
    ax = axes() #fig.add_subplot(111)
    # Create the boxplot

    pos = 2
    for values in data_to_plot:
        #print values
        bp = ax.boxplot(values, positions=[pos], widths=2)
        pos += 10

    xlim(0, 80)
    #ax.set_xticklabels(['NuSMV-bbd', 'nuXmv-ic3', 'NuSMV-bmc', 'iimc', 'aigbmc', 'nuxmv-bmc', 'nuXmv-bdd', 'IC3'])
    ax.set_title(model + '_allMC')
    ax.set_xticklabels(['iimc', 'nuXmv-ic3', 'aigbmc', 'IC3', 'nuXmv-bdd', 'nuxmv-bmc', 'NuSMV-bbd', 'NuSMV-bmc'])
    ax.set_xticks([2, 12, 22, 32, 42, 52, 62, 72])

#   bp = ax.boxplot(data_to_plot)
    # Save the figure
    now = datetime.datetime.now()
    fig.savefig(__out_path__ + model + '_allMC'+ now.strftime("%Y%m%d_%H%M") + '.png', bbox_inches='tight')
    fig.show()


def single_model_single_mc(model, model_checker):
    #
    # plot all single files
    #
    mc_time = [] # array for processing time data
    mc_time_split = []
    single_result_files = [file for file in os.listdir(__model_path__+ model  + "/" +  model_checker + "/") if (file.find("dccaresult_single") >= 0)]

    for single_result_file in single_result_files:
        mc_time_single = [] #processing time for each cycle
        mc_time_single_true  = [] #processing time for each cycle -> true
        mc_time_single_false = [] #processing time for each cycle -> false


        f = open(__model_path__ + model + "/" + model_checker + "/" + single_result_file)
        #skip first line
        f.readline()
        #read data
        lines = f.readlines()

        for l in lines:
            data_split = l.split(";")
            mc_time_single.append(float(data_split[2]))
            if data_split[1].find("1") >= 0:
                mc_time_single_false.append(float(data_split[2]))
            else:
                mc_time_single_true.append(float(data_split[2]))

        mc_time.append(mc_time_single)

        mc_time_split.append([mc_time_single_true,mc_time_single_false] )

        f.close()
    #
    #plot true and false together
    #
    data_to_plot = mc_time
    #  Create a figure instance
    fig = figure()#plt.figure(1)
    # Create an axes instance
    ax = axes() #fig.add_subplot(111)
    ax.set_ylabel("run time in s")
    ax.set_xlabel("test run")
    print mc_time
    ax.set_title(model + '_' + mc_dic[model_checker] +'_singleCheck')

    bp = ax.boxplot(data_to_plot)

    # Save the figure
    now = datetime.datetime.now()
    fig.savefig(__out_path__ + model + '_' + mc_dic[model_checker] +'_singleCheck_'+ now.strftime("%Y%m%d_%H%M") + '.png', bbox_inches='tight')


    #
    # plot true and false next to each other
    #
    fig_split = figure()
    ax_split = axes()
    hold(True)
    #fig_split = plt.figure(1, figsize=(9, 6))
    #ax_split = fig_split.add_subplot(111)
    pos = 1

    for values in mc_time_split:
        bp = ax_split.boxplot(values, positions=[pos, pos + 1], widths=1)
        pos += 6
        setBoxColors(bp)
 #   print mc_time_split

    # set axes limits and labels
    xlim(0, 60)
    #ylim(0,2)
    ax_split.set_xticklabels(['0', '1', '2', '3', '4', '5', '6', '7', '8', '9' ])
    ax_split.set_xticks([1.5, 7.5, 13.5, 19.5, 25.5, 31.5, 37.5, 43.5, 49.5, 55.5])

    # draw temporary red and blue lines and use them to create a legend
    hB, = plot([1,1],'b-')
    hR, = plot([1,1],'r-')
    legend((hB, hR),('true', 'CE'))
    hB.set_visible(False)
    hR.set_visible(False)
    ax.set_ylabel("run time in s")
    ax.set_xlabel("model checker")

    ax.set_title(model + '_' + mc_dic[model_checker] +'_singleCheckSplit')
    now = datetime.datetime.now()
    fig_split.savefig(__out_path__ + model + '_' + mc_dic[model_checker] +'_singleCheckSplit_'+ now.strftime("%Y%m%d_%H%M") + '.png', bbox_inches='tight')
    #
    #plot the overall times for each check
    #
    mc_time = []
    fig.clear()
    complete_result_file = [file for file in os.listdir(__model_path__+ model + "/" + model_checker + "/") if (file.find("dccaresult_complete") >= 0)]

    f = open(__model_path__ + model + "/" + model_checker + "/" + complete_result_file[0])
    f.readline()
    lines = f.readlines()

    for l in lines:
        data_split = l.split(";")
        mc_time.append([float(data_split[2])])

    data_to_plot = mc_time
    #  Create a figure instance
    fig = figure()#plt.figure(1, figsize=(9, 6))
    # Create an axes instance
    ax = axes()#fig.add_subplot(111)

    # Create the boxplot
    bp = ax.plot(data_to_plot, 'ro')
    # Save the figure
    ax.set_ylabel("run time in s")
    ax.set_xlabel("test run")

    ax.set_title(model + '_' + mc_dic[model_checker] +'_completeCheck')
    now = datetime.datetime.now()
    fig.savefig(__out_path__ + model + '_' + mc_dic[model_checker] +'_completeCheck_'+ now.strftime("%Y%m%d_%H%M") + '.png', bbox_inches='tight')


def setBoxColors(bp):
    setp(bp['boxes'][0], color='blue')
    setp(bp['caps'][0], color='blue')
    setp(bp['caps'][1], color='blue')
    setp(bp['whiskers'][0], color='blue')
    setp(bp['whiskers'][1], color='blue')
    setp(bp['fliers'][0], color='blue')
    setp(bp['fliers'][1], color='blue')
    setp(bp['medians'][0], color='blue')

    setp(bp['boxes'][1], color='red')
    setp(bp['caps'][2], color='red')
    setp(bp['caps'][3], color='red')
    setp(bp['whiskers'][2], color='red')
    setp(bp['whiskers'][3], color='red')
#    setp(bp['fliers'][2], color='red')
#    setp(bp['fliers'][3], color='red')
    setp(bp['medians'][1], color='red')


'''
' PROGRAM ENTRY POINT
'''
if __name__ == "__main__":
    main()
